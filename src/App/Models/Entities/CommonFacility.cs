﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using appartement_service_backend.App.Models.Dtos;

namespace appartement_service_backend.App.Models.Entities;

[Table("common_facilities")]
public class CommonFacility
{
    [Key]
    [Column("id")]
    [Required]
    public required int Id { get; set; }

    [Column("name")]
    [Required]
    public required string Name { get; set; }

    [Column("is_security_facility")]
    [Required]
    public required bool IsSecurityFacility { get; set; }

    [Column("last_verification_date")]
    public DateTime? LastVerificationDate { get; set; }

    public virtual ICollection<AssocBuildingCommonFacility>? AssocBuildingCommonFacilities { get; set; } = new List<AssocBuildingCommonFacility>();
    
    public CommonFacilityDto MapToDto()
    {
        var commonFacilityDto = new CommonFacilityDto
        {
            Id = Id,
            Name = Name,
            IsSecurityFacility = IsSecurityFacility,
            LastVerificationDate = LastVerificationDate
        };

        return commonFacilityDto;
    }
}