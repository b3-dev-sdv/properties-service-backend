﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace appartement_service_backend.App.Models.Entities;

[Table("assoc_building_common_facilities")]
[Keyless]
public class AssocBuildingCommonFacility
{
    [Column("building_id")]
    [Required]
    public required int BuildingId { get; set; }

    [Column("common_facility_id")]
    [Required]
    public required int CommonFacilityId { get; set; }

    [Column("last_verification_date")]
    [Required]
    public required DateTime LastVerificationDate { get; set; }

    [ForeignKey("BuildingId")]
    public required Building Building { get; set; }

    [ForeignKey("CommonFacilityId")]
    public required CommonFacility CommonFacility { get; set; }
}
