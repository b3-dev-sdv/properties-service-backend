﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using appartement_service_backend.App.Models.Dtos;

namespace appartement_service_backend.App.Models.Entities;

[Table("apartments")]
public class Apartment
{
    [Key]
    [Column("id")]
    [Required]
    public required int Id { get; set; }

    [Column("number")]
    [Required]
    public required int Number { get; set; }

    [Column("area")]
    public required int Area { get; set; }

    [Column("building_id")]
    [Required]
    public required int BuildingId { get; set; }

    [Column("apartment_type_id")]
    [Required]
    public required int ApartmentTypeId { get; set; }

    [Column("max_occupants")]
    public int MaxOccupants { get; set; }

    public required Building Building { get; set; }
    
    public required ApartmentType ApartmentType { get; set; }
    
    public ICollection<Tenant>? Tenants { get; set; } = new List<Tenant>();
    public ICollection<Appliance>? Appliances { get; set; } = new List<Appliance>();
    public ICollection<AssocApartmentsOptions>? AssocApartmentsOptions { get; set; } = new List<AssocApartmentsOptions>();

    public ApartmentDto MapToDto()
    {
        var apartmentDto = new ApartmentDto
        {
            Id = Id,
            Number = Number,
            Area = Area,
            MaxOccupants = MaxOccupants,
            Type = ApartmentType.MapToDto()
        };

        return apartmentDto;
    }
    
    public ApartmentDto MapToDetailsDto()
    {
        var apartmentDto = new ApartmentDto
        {
            Id = Id,
            Number = Number,
            Area = Area,
            MaxOccupants = MaxOccupants,
            Type = ApartmentType.MapToDto()
        };

        if (Tenants != null) apartmentDto.Tenants = Tenants.Select(o => o.MapToDto()).ToList();
        if (Appliances != null) apartmentDto.Appliances = Appliances.Select(o => o.MapToDto()).ToList();
        if (AssocApartmentsOptions != null) apartmentDto.Options = AssocApartmentsOptions.Select(o => o.Option.MapToDto()).ToList();
        
        return apartmentDto;
    }
}