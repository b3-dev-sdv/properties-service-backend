﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using appartement_service_backend.App.Models.Dtos;

namespace appartement_service_backend.App.Models.Entities;

[Table("options")]
public class Option
{
    [Key]
    [Column("id")]
    [Required]
    public required int Id { get; set; }

    [Column("name")]
    [Required]
    public required string Name { get; set; }

    public virtual ICollection<AssocApartmentsOptions>? AssocApartmentsOptions { get; set; } = new List<AssocApartmentsOptions>();
    
    public OptionDto MapToDto()
    {
        var optionDto = new OptionDto
        {
            Id = Id,
            Name = Name
        };

        return optionDto;
    }
}