﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using appartement_service_backend.App.Models.Dtos;

namespace appartement_service_backend.App.Models.Entities;

[Table("buildings")]
public class Building
{
    [Key] [Column("id")] [Required] public required int Id { get; set; }
    [Column("name")] [Required] public required string Name { get; set; }
    [Column("address")] [Required] public required string Address { get; set; }
    [Column("date_construction")] [Required] public required DateTime? DateConstruction { get; set; }

    
    public virtual ICollection<Apartment>? Apartments { get; set; } = new List<Apartment>();
    public virtual ICollection<AssocBuildingCommonFacility>? AssocBuildingCommonFacilities { get; set; } = new List<AssocBuildingCommonFacility>();
    
    public BuildingDto MapToDto()
    {
        var buildingDto = new BuildingDto
        {
            Id = Id,
            Name = Name,
            Address = Address,
            DateConstruction = DateConstruction
        };

        if (Apartments != null) buildingDto.Apartments = Apartments.Select(o => o.MapToDetailsDto()).ToList();
        if (AssocBuildingCommonFacilities != null)
            buildingDto.Facilities = AssocBuildingCommonFacilities.Select(o => o.CommonFacility.MapToDto()).ToList();
            
        return buildingDto;
    }
}