﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using appartement_service_backend.App.Models.Dtos;

namespace appartement_service_backend.App.Models.Entities;

[Table("appliances")]
public class Appliance
{
    [Key]
    [Column("id")]
    [Required]
    public int Id { get; set; }

    [Column("apartment_id")]
    [Required]
    public int ApartmentId { get; set; }

    [Column("name")]
    [Required]
    public required string Name { get; set; }

    [Column("appliance_type")]
    [Required]
    public required string ApplianceType { get; set; }
    
    public required Apartment Apartment { get; set; }
    
    public ApplianceDto MapToDto()
    {
        var applianceDto = new ApplianceDto
        {
            Id = Id,
            Name = Name,
            ApplianceType = ApplianceType,
            // Apartment = Apartment.MapToDto()
        };

        return applianceDto;
    }
}