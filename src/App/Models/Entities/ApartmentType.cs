﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using appartement_service_backend.App.Models.Dtos;

namespace appartement_service_backend.App.Models.Entities;

[Table("apartment_types")]
public class ApartmentType
{
    [Key]
    [Column("id")]
    [Required]
    public required int Id { get; set; }

    [Column("type")]
    [Required]
    public required string Type { get; set; }
    
    public ApartmentTypeDto MapToDto()
    {
        var apartmentDto = new ApartmentTypeDto
        {
            Id = Id,
            Type = Type
        };

        return apartmentDto;
    }
}