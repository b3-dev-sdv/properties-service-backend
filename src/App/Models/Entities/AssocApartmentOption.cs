﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace appartement_service_backend.App.Models.Entities;

[Table("assoc_apartments_options")]
[Keyless]
public class AssocApartmentsOptions
{
    [Column("apartment_id")]
    [Required]
    public required int ApartmentId { get; set; }

    [Column("option_id")]
    [Required]
    public required int OptionId { get; set; }

    [ForeignKey("ApartmentId")]
    public required Apartment Apartment { get; set; }

    [ForeignKey("OptionId")]
    public required Option Option { get; set; }
}