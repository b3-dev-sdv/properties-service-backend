﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using appartement_service_backend.App.Models.Dtos;

namespace appartement_service_backend.App.Models.Entities;

[Table("tenants")]
public class Tenant
{
    [Key]
    [Column("id")]
    [Required]
    public required int Id { get; set; }
    
    [Column("apartment_id")]
    [Required]
    public required int ApartmentId { get; set; }

    [Column("last_name")]
    [Required]
    public required string LastName { get; set; }

    [Column("first_name")]
    [Required]
    public required string FirstName { get; set; }

    [Column("is_guarantor")]
    [Required]
    public bool IsGuarantor { get; set; }
    
    public required Apartment Apartment { get; set; }
    
    public TenantDto MapToDto()
    {
        var tenantDto = new TenantDto
        {
            Id = Id,
            LastName = LastName,
            FirstName = FirstName,
            IsGuarantor = IsGuarantor
        };

        return tenantDto;
    }
}