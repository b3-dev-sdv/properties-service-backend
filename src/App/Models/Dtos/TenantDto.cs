﻿namespace appartement_service_backend.App.Models.Dtos;

public class TenantDto
{
    public int Id { get; set; }
    public required string LastName { get; set; }
    public required string FirstName { get; set; }
    public required bool IsGuarantor { get; set; }
}