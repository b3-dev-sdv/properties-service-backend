﻿namespace appartement_service_backend.App.Models.Dtos;

public class ApartmentTypeDto
{
    public int Id { get; set; }
    public required string Type { get; set; }
}