﻿namespace appartement_service_backend.App.Models.Dtos;

public class ApplianceDto
{
    public int Id { get; set; }
    public required string Name { get; set; }
    public required string ApplianceType { get; set; }
}