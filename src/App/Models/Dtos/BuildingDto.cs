﻿namespace appartement_service_backend.App.Models.Dtos;

public class BuildingDto
{
    public int Id { get; set; }
    public required string Name { get; set; }
    public required string Address { get; set; }
    public DateTime? DateConstruction { get; set; }
    public List<ApartmentDto>? Apartments { get; set; }
    public List<CommonFacilityDto>? Facilities { get; set; }
}