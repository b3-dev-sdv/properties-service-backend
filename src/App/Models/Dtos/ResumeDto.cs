﻿namespace appartement_service_backend.App.Models.Dtos;

public class ResumeDto
{
    public int TotalApartmentsNumber { get; set; }
    public double OccupancyRate { get; set; }
    public int TotalOccupants { get; set; }
    public int TotalUnderOccupiedApartments { get; set; }
    public int TotalOverOccupiedApartments { get; set; }
}