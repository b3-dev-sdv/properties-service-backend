﻿namespace appartement_service_backend.App.Models.Dtos;

public class OptionDto
{
    public int Id { get; set; }
    public required string Name { get; set; }
}