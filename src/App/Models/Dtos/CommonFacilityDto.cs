﻿namespace appartement_service_backend.App.Models.Dtos;

public class CommonFacilityDto
{
    public int Id { get; set; }
    public required string Name { get; set; }
    public bool IsSecurityFacility { get; set; }
    public DateTime? LastVerificationDate { get; set; }
}