﻿namespace appartement_service_backend.App.Models.Dtos;

public class ApartmentDto
{
    public int Id { get; set; }
    public required int Number { get; set; }
    public required int Area { get; set; }
    public required int MaxOccupants { get; set; }
    public ApartmentTypeDto? Type { get; set; }
    public List<TenantDto>? Tenants { get; set; }
    public List<OptionDto>? Options { get; set; }
    public List<ApplianceDto>? Appliances { get; set; }
}