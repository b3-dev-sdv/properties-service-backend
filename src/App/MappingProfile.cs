﻿using appartement_service_backend.App.Models.Dtos;
using appartement_service_backend.App.Models.Entities;
using AutoMapper;

namespace appartement_service_backend.App;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<BuildingDto, Building>();
        CreateMap<ApartmentDto, Apartment>();
        CreateMap<CommonFacilityDto, CommonFacility>();
        CreateMap<TenantDto, Tenant>();
        CreateMap<OptionDto, Option>();
        CreateMap<ApplianceDto, Appliance>();
    }
}
