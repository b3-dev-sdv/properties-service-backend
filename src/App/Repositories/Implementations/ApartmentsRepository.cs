﻿using appartement_service_backend.App.Contexts;
using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;
using Microsoft.EntityFrameworkCore;

namespace appartement_service_backend.App.Repositories.Implementations;

public class ApartmentsRepository(ApplicationDbContext context) : IApartmentsRepository
{
    public async Task<List<Apartment>> FindAll()
    {
        return await context.Apartments
            .Include(o => o.Appliances)
            .Include(o => o.Building)
            .ThenInclude(o => o.AssocBuildingCommonFacilities)!
            .ThenInclude(o => o.CommonFacility)
            .Include(o => o.Tenants)
            .Include(o => o.ApartmentType)
            .Include(o => o.AssocApartmentsOptions)
            .ThenInclude(o => o.Option)
            .ToListAsync();
    }

    public async Task<Apartment?> FindDetailsById(int id)
    {
        return await context.Apartments
            .Where(o => o.Id == id)
            .Include(o => o.Appliances)
            .Include(o => o.Building)
            .ThenInclude(o => o.AssocBuildingCommonFacilities)!
            .ThenInclude(o => o.CommonFacility)
            .Include(o => o.Tenants)
            .Include(o => o.ApartmentType)
            .Include(o => o.AssocApartmentsOptions)
            .ThenInclude(o => o.Option)
            .FirstAsync();
    }
    
    public async Task<Apartment?> FindById(int id)
    {
        return await context.Apartments
            .Where(o => o.Id == id)
            .Include(o => o.Tenants)
            .FirstAsync();
    }
    
    public async Task Save(Apartment apartment)
    {
        context.Apartments.Add(apartment);
        await context.SaveChangesAsync();
    }

    public async Task Update(Apartment apartment)
    {
        context.Entry(apartment).State = EntityState.Modified;
        await context.SaveChangesAsync();
    }

    public async Task DeleteById(int id)
    {
        var apartment = await context.Apartments.FindAsync(id);

        if (apartment != null)
        {
            context.Apartments.Remove(apartment);
            await context.SaveChangesAsync();
        }
    }

    public int CountAll()
    {
        return context.Apartments.Count();
    }

    public bool HasTenant(int apartmentId)
    {
        var apartment = FindById(apartmentId).Result;

        return apartment is { Tenants.Count: > 0 };
    }
    
    public async Task<List<Apartment>> FindAllByBuildingId(int buildingId)
    {
        return await context.Apartments
            .Where(o => o.BuildingId == buildingId)
            .Include(o => o.Appliances)
            .Include(o => o.Building)
            .ThenInclude(o => o.AssocBuildingCommonFacilities)!
            .ThenInclude(o => o.CommonFacility)
            .Include(o => o.Tenants)
            .Include(o => o.ApartmentType)
            .Include(o => o.AssocApartmentsOptions)
            .ThenInclude(o => o.Option)
            .ToListAsync();
    }
}