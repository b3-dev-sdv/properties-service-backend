﻿using appartement_service_backend.App.Contexts;
using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;
using Microsoft.EntityFrameworkCore;

namespace appartement_service_backend.App.Repositories.Implementations;

public class OptionsesRepository(ApplicationDbContext context) : IOptionsRepository
{
    public async Task<List<Option>> FindAll()
    {
        return await context.Options.ToListAsync();
    }

    public async Task<Option?> FindById(int id)
    {
        return await context.Options.FirstOrDefaultAsync(o => o.Id == id);
    }

    public async Task Save(Option option)
    {
        ArgumentNullException.ThrowIfNull(option);
        context.Options.Add(option);
        await context.SaveChangesAsync();
    }

    public async Task Update(Option option)
    {
        ArgumentNullException.ThrowIfNull(option);
        context.Entry(option).State = EntityState.Modified;
        await context.SaveChangesAsync();
    }

    public async Task DeleteById(int id)
    {
        var option = await context.Options.FindAsync(id);

        if (option != null)
        {
            context.Options.Remove(option);
            await context.SaveChangesAsync();
        }
    }
}