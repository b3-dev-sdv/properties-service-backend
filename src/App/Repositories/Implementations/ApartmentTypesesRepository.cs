﻿using appartement_service_backend.App.Contexts;
using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;
using Microsoft.EntityFrameworkCore;

namespace appartement_service_backend.App.Repositories.Implementations;

public class ApartmentTypesesRepository(ApplicationDbContext context) : IApartmentTypesRepository
{
    public async Task<List<ApartmentType>> FindAll()
    {
        return await context.ApartmentTypes.ToListAsync();
    }

    public async Task<ApartmentType?> FindById(int id)
    {
        return await context.ApartmentTypes.FirstOrDefaultAsync(t => t.Id == id);
    }

    public async Task Save(ApartmentType apartmentType)
    {
        ArgumentNullException.ThrowIfNull(apartmentType);
        context.ApartmentTypes.Add(apartmentType);
        await context.SaveChangesAsync();
    }

    public async Task Update(ApartmentType apartmentType)
    {
        ArgumentNullException.ThrowIfNull(apartmentType);
        context.Entry(apartmentType).State = EntityState.Modified;
        await context.SaveChangesAsync();
    }

    public async Task DeleteById(int id)
    {
        var apartmentType = await context.ApartmentTypes.FindAsync(id);

        if (apartmentType != null)
        {
            context.ApartmentTypes.Remove(apartmentType);
            await context.SaveChangesAsync();
        }
    }
}