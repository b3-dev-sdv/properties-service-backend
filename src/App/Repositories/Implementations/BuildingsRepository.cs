﻿using appartement_service_backend.App.Contexts;
using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;
using Microsoft.EntityFrameworkCore;

namespace appartement_service_backend.App.Repositories.Implementations;

public class BuildingsRepository(ApplicationDbContext context) : IBuildingsRepository
{
    public async Task<List<Building>> FindAll()
    {
        return await context.Buildings
            .Include(o => o.AssocBuildingCommonFacilities)!
            .ThenInclude(o => o.CommonFacility)
            .Include(o => o.Apartments)!
            .ThenInclude(o => o.Appliances)
            .Include(o => o.Apartments)
            .ThenInclude(o => o.Tenants)
            .Include(o => o.Apartments)
            .ThenInclude(o => o.Tenants)
            .Include(o => o.Apartments)
            .ThenInclude(o => o.ApartmentType)
            .Include(o => o.Apartments)
            .ThenInclude(o => o.AssocApartmentsOptions)
            .ThenInclude(o => o.Option)
            .ToListAsync();
    }

    public async Task<Building?> FindById(int id)
    {
        return await context.Buildings
            .Where(o => o.Id == id)
            .Include(o => o.AssocBuildingCommonFacilities)!
            .ThenInclude(o => o.CommonFacility)
            .Include(o => o.Apartments)!
            .ThenInclude(o => o.Appliances)
            .Include(o => o.Apartments)
            .ThenInclude(o => o.Tenants)
            .Include(o => o.Apartments)
            .ThenInclude(o => o.Tenants)
            .Include(o => o.Apartments)
            .ThenInclude(o => o.ApartmentType)
            .Include(o => o.Apartments)
            .ThenInclude(o => o.AssocApartmentsOptions)
            .ThenInclude(o => o.Option)
            .FirstAsync();
    }

    public async Task Save(Building building)
    {
        context.Buildings.Add(building);
        await context.SaveChangesAsync();
    }

    public async Task Update(Building building)
    {
        context.Entry(building).State = EntityState.Modified;
        await context.SaveChangesAsync();
    }

    public async Task DeleteById(int id)
    {
        var building = await context.Buildings.FindAsync(id);

        if (building != null)
        {
            context.Buildings.Remove(building);
            await context.SaveChangesAsync();
        }
    }
}