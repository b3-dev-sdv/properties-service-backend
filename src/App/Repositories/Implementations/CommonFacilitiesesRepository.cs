﻿using appartement_service_backend.App.Contexts;
using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;
using Microsoft.EntityFrameworkCore;

namespace appartement_service_backend.App.Repositories.Implementations;

public class CommonFacilitiesesRepository(ApplicationDbContext context) : ICommonFacilitiesRepository
{
    public async Task<List<CommonFacility>> FindAll()
    {
        return await context.CommonFacilities.ToListAsync();
    }

    public async Task<CommonFacility?> FindById(int id)
    {
        return await context.CommonFacilities.FirstOrDefaultAsync(cf => cf.Id == id);
    }

    public async Task Save(CommonFacility commonFacility)
    {
        ArgumentNullException.ThrowIfNull(commonFacility);
        context.CommonFacilities.Add(commonFacility);
        await context.SaveChangesAsync();
    }

    public async Task Update(CommonFacility commonFacility)
    {
        ArgumentNullException.ThrowIfNull(commonFacility);
        context.Entry(commonFacility).State = EntityState.Modified;
        await context.SaveChangesAsync();
    }

    public async Task DeleteById(int id)
    {
        var commonFacility = await context.CommonFacilities.FindAsync(id);

        if (commonFacility != null)
        {
            context.CommonFacilities.Remove(commonFacility);
            await context.SaveChangesAsync();
        }
    }
}