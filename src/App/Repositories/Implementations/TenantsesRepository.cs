﻿using appartement_service_backend.App.Contexts;
using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;
using Microsoft.EntityFrameworkCore;

namespace appartement_service_backend.App.Repositories.Implementations;

public class TenantsesRepository(ApplicationDbContext context) : ITenantsRepository
{
    public async Task<List<Tenant>> FindAll()
    {
        return await context.Tenants.ToListAsync();
    }

    public async Task<Tenant?> FindById(int id)
    {
        return await context.Tenants.FirstOrDefaultAsync(t => t.Id == id);
    }

    public async Task Save(Tenant tenant)
    {
        ArgumentNullException.ThrowIfNull(tenant);
        context.Tenants.Add(tenant);
        await context.SaveChangesAsync();
    }

    public async Task Update(Tenant tenant)
    {
        ArgumentNullException.ThrowIfNull(tenant);
        context.Entry(tenant).State = EntityState.Modified;
        await context.SaveChangesAsync();
    }

    public async Task DeleteById(int id)
    {
        var tenant = await context.Tenants.FindAsync(id);

        if (tenant != null)
        {
            context.Tenants.Remove(tenant);
            await context.SaveChangesAsync();
        }
    }

    public async Task<List<Tenant>> FindAllByApartmentId(int apartmentId)
    {
        return await context.Tenants.Where(o => o.ApartmentId == apartmentId).ToListAsync();
    }
}