﻿using appartement_service_backend.App.Contexts;
using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;
using Microsoft.EntityFrameworkCore;

namespace appartement_service_backend.App.Repositories.Implementations;

public class AppliancesRepository(ApplicationDbContext context) : IAppliancesRepository
{
    public async Task<List<Appliance>> FindAll()
    {
        return await context.Appliances
            .Include(o => o.Apartment)
            .ToListAsync();
    }

    public async Task<Appliance?> FindById(int id)
    {
        return await context.Appliances
            .Where(o => o.Id == id)
            .Include(o => o.Apartment)
            .FirstAsync();
    }

    public async Task Save(Appliance appliance)
    {
        context.Appliances.Add(appliance);
        await context.SaveChangesAsync();
    }

    public async Task Update(Appliance appliance)
    {
        context.Entry(appliance).State = EntityState.Modified;
        await context.SaveChangesAsync();
    }

    public async Task DeleteById(int id)
    {
        var appliance = await context.Appliances.FindAsync(id);

        if (appliance != null)
        {
            context.Appliances.Remove(appliance);
            await context.SaveChangesAsync();
        }
    }
}