﻿using appartement_service_backend.App.Models.Entities;

namespace appartement_service_backend.App.Repositories.interfaces;

public interface ITenantsRepository
{
    Task<List<Tenant>> FindAll();
    Task<Tenant?> FindById(int id);
    Task Save(Tenant commonFacility);
    Task Update(Tenant commonFacility);
    Task DeleteById(int id);
    Task<List<Tenant>> FindAllByApartmentId(int id);
}