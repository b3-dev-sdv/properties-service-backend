﻿using appartement_service_backend.App.Models.Entities;

namespace appartement_service_backend.App.Repositories.interfaces;

public interface IApartmentTypesRepository
{
    Task<List<ApartmentType>> FindAll();
    Task<ApartmentType?> FindById(int id);
    Task Save(ApartmentType apartmentType);
    Task Update(ApartmentType apartmentType);
    Task DeleteById(int id);
}