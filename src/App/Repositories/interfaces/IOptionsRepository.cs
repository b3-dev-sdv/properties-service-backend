﻿using appartement_service_backend.App.Models.Entities;

namespace appartement_service_backend.App.Repositories.interfaces;

public interface IOptionsRepository
{
    Task<List<Option>> FindAll();
    Task<Option?> FindById(int id);
    Task Save(Option commonFacility);
    Task Update(Option commonFacility);
    Task DeleteById(int id);
}