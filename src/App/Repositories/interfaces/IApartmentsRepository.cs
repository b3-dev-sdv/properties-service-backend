﻿using appartement_service_backend.App.Models.Entities;

namespace appartement_service_backend.App.Repositories.interfaces;

public interface IApartmentsRepository
{
    Task<List<Apartment>> FindAll();
    Task<Apartment?> FindDetailsById(int id);
    Task Save(Apartment apartment);
    Task Update(Apartment apartment);
    Task DeleteById(int id);
    Task<List<Apartment>> FindAllByBuildingId(int buildingId);
    int CountAll();
    bool HasTenant(int apartmentId);
}