﻿using appartement_service_backend.App.Models.Entities;

namespace appartement_service_backend.App.Repositories.interfaces;

public interface IBuildingsRepository
{
    Task<List<Building>> FindAll();
    Task<Building?> FindById(int id);
    Task Save(Building building);
    Task Update(Building building);
    Task DeleteById(int id);
}