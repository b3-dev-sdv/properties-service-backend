﻿using appartement_service_backend.App.Models.Entities;

namespace appartement_service_backend.App.Repositories.interfaces;

public interface IAppliancesRepository
{
    Task<List<Appliance>> FindAll();
    Task<Appliance?> FindById(int id);
    Task Save(Appliance appliance);
    Task Update(Appliance appliance);
    Task DeleteById(int id);
}