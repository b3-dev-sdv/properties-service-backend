﻿using appartement_service_backend.App.Models.Entities;

namespace appartement_service_backend.App.Repositories.interfaces;

public interface ICommonFacilitiesRepository
{
    Task<List<CommonFacility>> FindAll();
    Task<CommonFacility?> FindById(int id);
    Task Save(CommonFacility commonFacility);
    Task Update(CommonFacility commonFacility);
    Task DeleteById(int id);
}