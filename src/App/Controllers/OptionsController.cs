﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Services;
using Microsoft.AspNetCore.Mvc;

namespace appartement_service_backend.App.Controllers;

[ApiController]
[Route("api/[controller]")]
public class OptionsController(OptionsService optionsService) : ControllerBase
{
    private readonly OptionsService _optionsService = optionsService ?? throw new ArgumentNullException(nameof(optionsService));

    [HttpGet]
    public async Task<ActionResult<List<Option>>> FindAll()
    {
        try
        {
            var options = await _optionsService.FindAll();
            var optionsDtos = options.Select(o => o.MapToDto()).ToList();
            
            return Ok(optionsDtos);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de toutes les options : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<Option>> FindById(int id)
    {
        try
        {
            var option = await _optionsService.FindById(id);

            if (option == null)
            {
                return NotFound();
            }

            var optionDto = option.MapToDto();

            return Ok(optionDto);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de l'option par ID : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPost]
    public async Task<ActionResult> Save([FromBody] Option option)
    {
        try
        {
            await _optionsService.Save(option);
            
            return CreatedAtAction(nameof(FindById), new { id = option.Id }, option);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de l'ajout de l'option : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPut("{id:int}")]
    public async Task<ActionResult> UpdateOption(int id, [FromBody] Option option)
    {
        try
        {
            if (id != option.Id)
            {
                return BadRequest();
            }

            await _optionsService.Update(option);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la mise à jour de l'option : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpDelete("{id:int}")]
    public async Task<ActionResult> DeleteById(int id)
    {
        try
        {
            await _optionsService.DeleteById(id);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la suppression de l'option : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }
}