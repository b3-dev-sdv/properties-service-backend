﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Services;
using Microsoft.AspNetCore.Mvc;

namespace appartement_service_backend.App.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TenantsController(TenantsService tenantsService) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<List<Tenant>>> FindAll()
    {
        try
        {
            var tenants = await tenantsService.FindAll();
            var tenantsDto = tenants.Select(o => o.MapToDto()).ToList();
            
            return Ok(tenantsDto);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de tous les locataires : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<Tenant>> FindById(int id)
    {
        try
        {
            var tenant = await tenantsService.FindById(id);

            if (tenant == null)
            {
                return NotFound();
            }

            return Ok(tenant);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération du locataire par ID : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPost]
    public async Task<ActionResult> Save([FromBody] Tenant tenant)
    {
        try
        {
            await tenantsService.Save(tenant);
            
            return CreatedAtAction(nameof(FindById), new { id = tenant.Id }, tenant);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de l'ajout du locataire : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPut("{id:int}")]
    public async Task<ActionResult> Update(int id, [FromBody] Tenant tenant)
    {
        try
        {
            if (id != tenant.Id)
            {
                return BadRequest();
            }

            await tenantsService.Update(tenant);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la mise à jour du locataire : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpDelete("{id:int}")]
    public async Task<ActionResult> DeleteById(int id)
    {
        try
        {
            await tenantsService.DeleteById(id);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la suppression du locataire : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }
}