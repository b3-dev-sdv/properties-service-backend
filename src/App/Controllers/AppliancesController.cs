﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Services;
using Microsoft.AspNetCore.Mvc;

namespace appartement_service_backend.App.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AppliancesController(AppliancesService appliancesService) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<List<Appliance>>> FindAll()
    {
        try
        {
            var appliances = await appliancesService.FindAll();
            var appliancesDtos = appliances.Select(o => o.MapToDto());
            
            return Ok(appliancesDtos);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de tous les appareils : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<Appliance>> FindById(int id)
    {
        try
        {
            var appliance = await appliancesService.FindById(id);

            if (appliance == null)
            {
                return NotFound();
            }

            return Ok(appliance);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de l'appareil par ID : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPost]
    public async Task<ActionResult> Save([FromBody] Appliance appliance)
    {
        try
        {
            await appliancesService.Save(appliance);
            
            return CreatedAtAction(nameof(FindById), new { id = appliance.Id }, appliance);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de l'ajout de l'appareil : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPut("{id:int}")]
    public async Task<ActionResult> Update(int id, [FromBody] Appliance appliance)
    {
        try
        {
            if (id != appliance.Id)
            {
                return BadRequest();
            }

            await appliancesService.Update(appliance);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la mise à jour de l'appareil : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpDelete("{id:int}")]
    public async Task<ActionResult> DeleteById(int id)
    {
        try
        {
            await appliancesService.Delete(id);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la suppression de l'appareil : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }
}