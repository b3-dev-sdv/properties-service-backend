﻿using appartement_service_backend.App.Models.Dtos;
using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace appartement_service_backend.App.Controllers;

[ApiController]
[Route("api/[controller]")]
public class BuildingsController(BuildingsService buildingsService, IMapper mapper) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<List<Building>>> FindAll()
    {
        try
        {
            var buildings = await buildingsService.FindAll();
            var buildingsDto = buildings.Select(o => o.MapToDto()).ToList();
            
            return Ok(buildingsDto);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de tous les immeubles : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<Building>> FindById(int id)
    {
        try
        {
            var building = await buildingsService.FindById(id);

            if (building == null)
            {
                return NotFound();
            }

            var buildingDto = building.MapToDto();
            
            return Ok(buildingDto);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de l'immeuble par ID : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPost]
    public async Task<ActionResult> Save([FromBody] BuildingDto buildingDto)
    {
        try
        {
            var building = mapper.Map<Building>(buildingDto);
            await buildingsService.Save(building);
            
            return CreatedAtAction(nameof(FindById), new { id = building.Id }, building);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de l'ajout de l'immeuble : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPut("{id:int}")]
    public async Task<ActionResult> Update(int id, [FromBody] BuildingDto buildingDto)
    {
        try
        {
            var building = mapper.Map<Building>(buildingDto);

            if (id != building.Id)
            {
                return BadRequest();
            }

            await buildingsService.Update(building);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la mise à jour de l'immeuble : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpDelete("{id:int}")]
    public async Task<ActionResult> DeleteById(int id)
    {
        try
        {
            await buildingsService.DeleteById(id);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la suppression de l'immeuble : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }
}