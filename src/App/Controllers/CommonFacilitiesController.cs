﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Services;
using Microsoft.AspNetCore.Mvc;

namespace appartement_service_backend.App.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CommonFacilitiesController(CommonFacilitiesService commonFacilitiesService) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<List<CommonFacility>>> FindAll()
    {
        try
        {
            var commonFacilities = await commonFacilitiesService.FindAll();
            var commonFacilitiesDtos = commonFacilities.Select(o => o.MapToDto()).ToList();
            
            return Ok(commonFacilitiesDtos);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de toutes les installations communes : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<CommonFacility>> FindById(int id)
    {
        try
        {
            var commonFacility = await commonFacilitiesService.FindById(id);

            if (commonFacility == null)
            {
                return NotFound();
            }

            return Ok(commonFacility);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de l'installation commune par ID : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPost]
    public async Task<ActionResult> Save([FromBody] CommonFacility commonFacility)
    {
        try
        {
            await commonFacilitiesService.Save(commonFacility);
            
            return CreatedAtAction(nameof(FindById), new { id = commonFacility.Id }, commonFacility);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de l'ajout de l'installation commune : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPut("{id:int}")]
    public async Task<ActionResult> Update(int id, [FromBody] CommonFacility commonFacility)
    {
        try
        {
            if (id != commonFacility.Id)
            {
                return BadRequest();
            }

            await commonFacilitiesService.Update(commonFacility);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la mise à jour de l'installation commune : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpDelete("{id:int}")]
    public async Task<ActionResult> DeleteById(int id)
    {
        try
        {
            await commonFacilitiesService.DeleteById(id);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la suppression de l'installation commune : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }
}