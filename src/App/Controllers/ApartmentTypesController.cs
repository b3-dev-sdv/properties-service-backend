﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Services;
using Microsoft.AspNetCore.Mvc;

namespace appartement_service_backend.App.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ApartmentTypesController(ApartmentTypesService apartmentTypesService) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<IEnumerable<ApartmentType>>> FindAll()
    {
        var apartmentTypes = await apartmentTypesService.FindAll();
        var apartmentTypesDtos = apartmentTypes.Select(o => o.MapToDto()).ToList();
        
        return Ok(apartmentTypesDtos);
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<ApartmentType>> FindById(int id)
    {
        var apartmentType = await apartmentTypesService.FindById(id);

        if (apartmentType == null)
        {
            return NotFound();
        }

        var apartmentTypeDto = apartmentType.MapToDto();

        return Ok(apartmentTypeDto);
    }

    [HttpPost]
    public async Task<ActionResult<ApartmentType>> Save(ApartmentType apartmentType)
    {
        await apartmentTypesService.Save(apartmentType);

        return CreatedAtAction(nameof(FindById), new { id = apartmentType.Id }, apartmentType);
    }

    [HttpPut("{id:int}")]
    public async Task<IActionResult> UpdateApartmentType(int id, ApartmentType apartmentType)
    {
        if (id != apartmentType.Id)
        {
            return BadRequest();
        }

        await apartmentTypesService.Update(apartmentType);

        return NoContent();
    }

    [HttpDelete("{id:int}")]
    public async Task<IActionResult> DeleteApartmentType(int id)
    {
        var apartmentType = await apartmentTypesService.FindById(id);
        if (apartmentType == null)
        {
            return NotFound();
        }

        await apartmentTypesService.DeleteById(apartmentType.Id);

        return NoContent();
    }
}