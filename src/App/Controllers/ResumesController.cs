﻿using appartement_service_backend.App.Models.Dtos;
using appartement_service_backend.App.Services;
using Microsoft.AspNetCore.Mvc;

namespace appartement_service_backend.App.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ResumesController(ResumesService resumesService) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<ResumeDto>> FindGlobalResume()
    {
        try
        {
            var resume = await resumesService.FindGlobalResume();
            
            return Ok(resume);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération du résumé global : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpGet("buildings/{id:int}")]
    public async Task<ActionResult<ResumeDto>> FindBuildingResume(int id)
    {
        try
        {
            var resume = await resumesService.FindBuildingResume(id);
            
            return Ok(resume);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération du résumé du bâtiment : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }
}