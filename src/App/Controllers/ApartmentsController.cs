﻿using appartement_service_backend.App.Models.Dtos;
using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace appartement_service_backend.App.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ApartmentsController(ApartmentsService apartmentsService, IMapper mapper) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<List<Apartment>>> FindAll()
    {
        try
        {
            var apartments = await apartmentsService.FindAll();
            var apartmentsDtos = apartments.Select(o => o.MapToDetailsDto()).ToList();
            
            return Ok(apartmentsDtos);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération des appartements : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<Apartment>> FindById(int id)
    {
        try
        {
            var apartment = await apartmentsService.FindById(id);

            if (apartment == null)
            {
                return NotFound();
            }
            
            var apartmentDto = apartment.MapToDetailsDto();

            return Ok(apartmentDto);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de l'appartement par ID : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPost]
    public async Task<ActionResult> Save([FromBody] ApartmentDto apartmentDto)
    {
        try
        {
            var apartment = mapper.Map<Apartment>(apartmentDto);
            await apartmentsService.Save(apartment);
            
            return CreatedAtAction(nameof(FindById), new { id = apartment.Id }, apartment);
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de l'ajout de l'appartement : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpPut("{id:int}")]
    public async Task<ActionResult> Update(int id, [FromBody] ApartmentDto apartmentDto)
    {
        try
        {
            var apartment = mapper.Map<Apartment>(apartmentDto);

            if (id != apartment.Id)
            {
                return BadRequest();
            }

            await apartmentsService.Update(apartment);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la mise à jour de l'appartement : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }

    [HttpDelete("{id:int}")]
    public async Task<ActionResult> DeleteById(int id)
    {
        try
        {
            await apartmentsService.DeleteById(id);
            
            return NoContent();
        }
        
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la suppression de l'appartement : {ex.Message}");
            
            return StatusCode(500, "Erreur serveur interne");
        }
    }
}