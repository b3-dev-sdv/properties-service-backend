﻿using appartement_service_backend.App.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace appartement_service_backend.App.Contexts;

public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : DbContext(options)
{
    public DbSet<Building> Buildings => Set<Building>();
    public DbSet<Apartment> Apartments => Set<Apartment>();
    public DbSet<Appliance> Appliances => Set<Appliance>();
    public DbSet<Option> Options => Set<Option>();
    public DbSet<Tenant> Tenants => Set<Tenant>();
    public DbSet<CommonFacility> CommonFacilities => Set<CommonFacility>();
    public DbSet<ApartmentType> ApartmentTypes => Set<ApartmentType>();
    public IEnumerable<AssocApartmentsOptions> ApartmentsOptions => Set<AssocApartmentsOptions>();
    public IEnumerable<AssocBuildingCommonFacility> AssocBuildingCommonFacilities => Set<AssocBuildingCommonFacility>();

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AssocApartmentsOptions>()
            .HasKey(assocApartmentsOptions => new { assocApartmentsOptions.ApartmentId, assocApartmentsOptions.OptionId });

        modelBuilder.Entity<AssocApartmentsOptions>()
            .HasOne(assocApartmentsOptions => assocApartmentsOptions.Apartment)
            .WithMany(apartment => apartment.AssocApartmentsOptions)
            .HasForeignKey(assocApartmentsOptions => assocApartmentsOptions.ApartmentId);

        modelBuilder.Entity<AssocApartmentsOptions>()
            .HasOne(assocApartmentsOptions => assocApartmentsOptions.Option)
            .WithMany(option => option.AssocApartmentsOptions)
            .HasForeignKey(assocApartmentsOptions => assocApartmentsOptions.OptionId);
        
        modelBuilder.Entity<AssocBuildingCommonFacility>()
            .HasKey(assocBuildingCommonFacility => new { assocBuildingCommonFacility.BuildingId, assocBuildingCommonFacility.CommonFacilityId });

        modelBuilder.Entity<AssocBuildingCommonFacility>()
            .HasOne(assocBuildingCommonFacility => assocBuildingCommonFacility.Building)
            .WithMany(building => building.AssocBuildingCommonFacilities)
            .HasForeignKey(assocBuildingCommonFacility => assocBuildingCommonFacility.BuildingId);

        modelBuilder.Entity<AssocBuildingCommonFacility>()
            .HasOne(assocBuildingCommonFacility => assocBuildingCommonFacility.CommonFacility)
            .WithMany(commonFacility => commonFacility.AssocBuildingCommonFacilities)
            .HasForeignKey(commonFacility => commonFacility.CommonFacilityId);
    }
}