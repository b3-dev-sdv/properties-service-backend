﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;

namespace appartement_service_backend.App.Services;

public class OptionsService(IOptionsRepository optionsRepository)
{
    public async Task<List<Option>> FindAll()
    {
        return await optionsRepository.FindAll();
    }

    public async Task<Option?> FindById(int id)
    {
        return await optionsRepository.FindById(id);
    }

    public async Task Save(Option option)
    {
        await optionsRepository.Save(option);
    }

    public async Task Update(Option option)
    {
        await optionsRepository.Update(option);
    }

    public async Task DeleteById(int id)
    {
        await optionsRepository.DeleteById(id);
    }
}