﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;

namespace appartement_service_backend.App.Services;

public class BuildingsService(IBuildingsRepository buildingsRepository)
{
    public async Task<List<Building>> FindAll()
    {
        try
        {
            return await buildingsRepository.FindAll();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de tous les immeubles : {ex.Message}");
            return [];
        }
    }

    public async Task<Building?> FindById(int id)
    {
        try
        {
            return await buildingsRepository.FindById(id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de l'immeuble par ID : {ex.Message}");
            return null;
        }
    }

    public async Task Save(Building building)
    {
        try
        {
            await buildingsRepository.Save(building);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de l'ajout de l'immeuble : {ex.Message}");
        }
    }

    public async Task Update(Building building)
    {
        try
        {
            await buildingsRepository.Update(building);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la mise à jour de l'immeuble : {ex.Message}");
        }
    }

    public async Task DeleteById(int id)
    {
        try
        {
            await buildingsRepository.DeleteById(id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la suppression de l'immeuble : {ex.Message}");
        }
    }
}