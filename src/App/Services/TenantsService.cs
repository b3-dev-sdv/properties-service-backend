﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;

namespace appartement_service_backend.App.Services;

public class TenantsService(ITenantsRepository tenantsRepository)
{
    public async Task<List<Tenant>> FindAll()
    {
        return await tenantsRepository.FindAll();
    }

    public async Task<Tenant?> FindById(int id)
    {
        return await tenantsRepository.FindById(id);
    }

    public async Task Save(Tenant tenant)
    {
        await tenantsRepository.Save(tenant);
    }

    public async Task Update(Tenant tenant)
    {
        if (tenant.IsGuarantor)
        {
            var apartmentTenants = await tenantsRepository.FindAllByApartmentId(tenant.ApartmentId);
            var otherTenants = apartmentTenants.Where(t => t.Id != tenant.Id);

            foreach (var otherTenant in otherTenants)
            {
                otherTenant.IsGuarantor = false;
                await tenantsRepository.Update(otherTenant);
            }
        }

        await tenantsRepository.Update(tenant);
    }

    public async Task DeleteById(int id)
    {
        var tenantToDelete = await tenantsRepository.FindById(id);

        if (tenantToDelete == null)
        {
            throw new InvalidOperationException("Locataire non trouvé");
        }

        if (tenantToDelete.IsGuarantor)
        {
            throw new InvalidOperationException("Un locataire garant ne peut pas être supprimé.");
        }

        await tenantsRepository.DeleteById(id);
    }


    public async Task<List<Tenant>> FindAllByApartmentId(int apartmentId)
    {
        return await tenantsRepository.FindAllByApartmentId(apartmentId);
    }
}