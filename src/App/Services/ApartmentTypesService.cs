﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;

namespace appartement_service_backend.App.Services;

public class ApartmentTypesService(IApartmentTypesRepository apartmentTypesRepository)
{
    private readonly IApartmentTypesRepository _apartmentTypesRepository = apartmentTypesRepository ?? throw new ArgumentNullException(nameof(apartmentTypesRepository));

    public async Task<List<ApartmentType>> FindAll()
    {
        return await _apartmentTypesRepository.FindAll();
    }

    public async Task<ApartmentType?> FindById(int id)
    {
        return await _apartmentTypesRepository.FindById(id);
    }

    public async Task Save(ApartmentType apartmentType)
    {
        await _apartmentTypesRepository.Save(apartmentType);
    }

    public async Task Update(ApartmentType apartmentType)
    {
        await _apartmentTypesRepository.Update(apartmentType);
    }

    public async Task DeleteById(int id)
    {
        await _apartmentTypesRepository.DeleteById(id);
    }
}