﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;

namespace appartement_service_backend.App.Services;

public abstract class CommonFacilitiesService(ICommonFacilitiesRepository commonFacilitiesRepository, BuildingsService buildingsService)
{
    public async Task<List<CommonFacility>> FindAll()
    {
        return await commonFacilitiesRepository.FindAll();
    }

    public async Task<CommonFacility?> FindById(int id)
    {
        return await commonFacilitiesRepository.FindById(id);
    }

    public async Task Save(CommonFacility commonFacility)
    {
        if (commonFacility.LastVerificationDate == null)
        {
            var building = await buildingsService.FindById(commonFacility.Id);
            commonFacility.LastVerificationDate = building?.DateConstruction;
        }

        await commonFacilitiesRepository.Save(commonFacility);
    }

    public async Task Update(CommonFacility commonFacility)
    {
        // Mettez à jour la date de vérification lors de la mise à jour
        commonFacility.LastVerificationDate = DateTime.Now;
        await commonFacilitiesRepository.Update(commonFacility);
    }

    public async Task DeleteById(int id)
    {
        await commonFacilitiesRepository.DeleteById(id);
    }
}