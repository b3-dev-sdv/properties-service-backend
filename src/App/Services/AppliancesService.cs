﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;

namespace appartement_service_backend.App.Services;

public class AppliancesService(IAppliancesRepository appliancesRepository)
{
    public async Task<List<Appliance>> FindAll()
    {
        try
        {
            return await appliancesRepository.FindAll();
        }
        catch (Exception ex)
        {
            // Gérer l'exception selon vos besoins (journalisation, renvoyer une liste vide, etc.)
            Console.WriteLine($"Erreur lors de la récupération de tous les appareils : {ex.Message}");
            return [];
        }
    }

    public async Task<Appliance?> FindById(int id)
    {
        try
        {
            return await appliancesRepository.FindById(id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de l'appareil par ID : {ex.Message}");
            return null;
        }
    }

    public async Task Save(Appliance appareil)
    {
        try
        {
            await appliancesRepository.Save(appareil);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de l'ajout de l'appareil : {ex.Message}");
        }
    }

    public async Task Update(Appliance appareil)
    {
        try
        {
            await appliancesRepository.Update(appareil);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la mise à jour de l'appareil : {ex.Message}");
        }
    }

    public async Task Delete(int id)
    {
        try
        {
            await appliancesRepository.DeleteById(id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la suppression de l'appareil : {ex.Message}");
        }
    }
}