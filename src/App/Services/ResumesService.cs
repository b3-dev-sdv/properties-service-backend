﻿using appartement_service_backend.App.Models.Dtos;
using appartement_service_backend.App.Repositories.interfaces;

namespace appartement_service_backend.App.Services;

public class ResumesService(IApartmentsRepository apartmentsRepository)
{
    public async Task<ResumeDto> FindGlobalResume()
    {
        var totalApartmentsNumber = apartmentsRepository.CountAll();
        var apartments = await apartmentsRepository.FindAll();
        var totalOccupiedApartments = apartments.Count(a => a.Tenants != null && a.Tenants.Count != 0);
        var totalOccupants = apartments.SelectMany(a => a.Tenants ?? throw new InvalidOperationException()).Count();
        var totalUnderOccupiedApartments = apartments.Count(a => a.Tenants != null && a.Tenants.Count < a.MaxOccupants);
        var totalOverOccupiedApartments = apartments.Count(a => a.Tenants != null && a.Tenants.Count > a.MaxOccupants);
        var occupancyRate = totalOccupiedApartments > 0 ? (double)totalOccupiedApartments / totalApartmentsNumber * 100 : 0;

        var resume = new ResumeDto
        {
            TotalApartmentsNumber = totalApartmentsNumber,
            OccupancyRate = occupancyRate,
            TotalOccupants = totalOccupants,
            TotalUnderOccupiedApartments = totalUnderOccupiedApartments,
            TotalOverOccupiedApartments = totalOverOccupiedApartments
        };

        return resume;
    }

    public async Task<ResumeDto> FindBuildingResume(int buildingId)
    {
        var buildingApartments = await apartmentsRepository.FindAllByBuildingId(buildingId);
        var totalApartmentsNumber = buildingApartments.Count;
        var totalOccupiedApartments = buildingApartments.Count(a => a.Tenants != null && a.Tenants.Count != 0);
        var totalOccupants = buildingApartments.SelectMany(a => a.Tenants ?? throw new InvalidOperationException()).Count();
        var totalUnderOccupiedApartments = buildingApartments.Count(a => a.Tenants != null && a.Tenants.Count < a.MaxOccupants);
        var totalOverOccupiedApartments = buildingApartments.Count(a => a.Tenants != null && a.Tenants.Count > a.MaxOccupants);
        var occupancyRate = totalOccupiedApartments > 0 ? (double)totalOccupiedApartments / totalApartmentsNumber * 100 : 0;

        var resume = new ResumeDto
        {
            TotalApartmentsNumber = totalApartmentsNumber,
            OccupancyRate = occupancyRate,
            TotalOccupants = totalOccupants,
            TotalUnderOccupiedApartments = totalUnderOccupiedApartments,
            TotalOverOccupiedApartments = totalOverOccupiedApartments
        };

        return resume;
    }
}
