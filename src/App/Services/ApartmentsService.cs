﻿using appartement_service_backend.App.Models.Entities;
using appartement_service_backend.App.Repositories.interfaces;

namespace appartement_service_backend.App.Services;

public class ApartmentsService(IApartmentsRepository apartmentsRepository)
{
    public async Task<List<Apartment>> FindAll()
    {
        try
        {
            return await apartmentsRepository.FindAll();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de tous les appartements : {ex.Message}");
            return [];
        }
    }

    public async Task<Apartment?> FindById(int id)
    {
        try
        {
            return await apartmentsRepository.FindDetailsById(id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la récupération de l'appartement par ID : {ex.Message}");
            return null;
        }
    }

    public async Task Save(Apartment apartment)
    {
        try
        {
            await apartmentsRepository.Save(apartment);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de l'ajout de l'appartement : {ex.Message}");
        }
    }

    public async Task Update(Apartment apartment)
    {
        try
        {
            await apartmentsRepository.Update(apartment);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la mise à jour de l'appartement : {ex.Message}");
        }
    }

    public async Task DeleteById(int id)
    {
        try
        {
            await apartmentsRepository.DeleteById(id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Erreur lors de la suppression de l'appartement : {ex.Message}");
        }
    }
    
    public int CountAll()
    {
        return apartmentsRepository.CountAll();
    }

    public bool HasTenant(int apartmentId)
    {
        return apartmentsRepository.HasTenant(apartmentId);
    }
}