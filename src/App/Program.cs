using appartement_service_backend.App;
using appartement_service_backend.App.Contexts;
using appartement_service_backend.App.Repositories.Implementations;
using appartement_service_backend.App.Repositories.interfaces;
using appartement_service_backend.App.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IBuildingsRepository, BuildingsRepository>();
builder.Services.AddScoped<IApartmentsRepository, ApartmentsRepository>();
builder.Services.AddScoped<IAppliancesRepository, AppliancesRepository>();
builder.Services.AddScoped<BuildingsService>();
builder.Services.AddScoped<ApartmentsService>();
builder.Services.AddScoped<AppliancesService>();
builder.Services.AddScoped<ResumesService>();
builder.Services.AddAutoMapper(typeof(MappingProfile));

builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseMySQL(builder.Configuration.GetConnectionString("DefaultConnection") ?? throw new InvalidOperationException(),
        optionsBuilder => optionsBuilder.UseQuerySplittingBehavior(default)));
builder.Services.AddSingleton<IConfiguration>(builder.Configuration);

builder.Services.AddControllers().AddNewtonsoftJson(options =>
    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);

var app = builder.Build();

// if (app.Environment.IsDevelopment())
// {
//
// }

app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();
app.MapControllers();
app.Run();